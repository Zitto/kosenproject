﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ControlCharacter : MonoBehaviour
{
    public GameObject Player;
    public float DISTANCE_MOVE_UPPER;
    public float DISTANCE_MOVE_LOWER;
    public float DISTANCE_MOVE_RIGHT;
    public float DISTANCE_MOVE_LEFT;

    private Vector3 Pos_origin_pos;

    // Start is called before the first frame update
    void Start()
    {
        Pos_origin_pos = Player.transform.position;
    }

    // Update is called once per frame
    void Update()
    {
    }

    public void Click_button_upper()
    {
        Pos_origin_pos = Player.transform.position;
        Player.transform.position = new Vector3(Pos_origin_pos.x, Pos_origin_pos.y, Pos_origin_pos.z + DISTANCE_MOVE_UPPER);
    }
    public void Click_button_lower()
    {
        Pos_origin_pos = Player.transform.position;
        Player.transform.position = new Vector3(Pos_origin_pos.x, Pos_origin_pos.y, Pos_origin_pos.z - DISTANCE_MOVE_LOWER);
    }
    public void Click_button_right()
    {
        Pos_origin_pos = Player.transform.position;
        Player.transform.position = new Vector3(Pos_origin_pos.x+ DISTANCE_MOVE_RIGHT, Pos_origin_pos.y, Pos_origin_pos.z );
    }
    public void Click_button_left()
    {
        Pos_origin_pos = Player.transform.position;
        Player.transform.position = new Vector3(Pos_origin_pos.x - DISTANCE_MOVE_LEFT, Pos_origin_pos.y, Pos_origin_pos.z);
    }
}
