package com.example.quizsample;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.Resources;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Debug;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;

import java.util.ArrayList;
import java.util.Collections;

public class MainActivity extends AppCompatActivity {

    private static final int NUM_OPTIONS_ARRAY_MAX = 2;
    ImageView _Iv_quiz;
    Button _Button_option_1;
    Button _Button_option_2;
    Button _Button_option_3;

    TypedArray T_Array_images;  //画像リストのグローバル宣言
    TypedArray T_Array_options_wrong; //選択肢リストのグローバル宣言
    TypedArray T_Array_options_right; //正解選択肢リストのグローバル宣言


    ArrayList<String> Array_options_wrong = new ArrayList<String>();    //不正解選択肢を格納する配列
    ArrayList<String> Option_texts = new ArrayList<String>();   //ボタンにセットする選択肢を入れる配列の作成

    int id_random_image;
    String option_right;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        _Iv_quiz = findViewById(R.id.iv_quiz);
        _Button_option_1 = findViewById(R.id.button1);
        _Button_option_2 = findViewById(R.id.button2);
        _Button_option_3 = findViewById(R.id.button3);

        //画像リスト作成
        T_Array_images = getResources().obtainTypedArray(R.array.list_random_image);
        //選択肢リスト作成
        T_Array_options_wrong = getResources().obtainTypedArray(R.array.optionsArray);
        for(int i=0; i<=NUM_OPTIONS_ARRAY_MAX; i++) {   //TypedArrayでは行の挿入削除が行えないためArrayListに移す
            Array_options_wrong.add(T_Array_options_wrong.getString(i));
        }
        //正解選択肢リスト作成
        T_Array_options_right = getResources().obtainTypedArray(R.array.ArrayRightOption);

        //Math.floorは少数切り捨て
        id_random_image = (int)Math.floor(Math.random() * 3);    //画像選択用ランダムid生成
        decide_options(id_random_image);    //正解選択肢、不正解選択肢1、不正解選択肢2を決定

        Drawable drawable = T_Array_images.getDrawable(id_random_image);  //ランダムに画像を取得
        _Iv_quiz.setImageDrawable(drawable);    //ランダムに選ばれた画像を設定
        set_button_text();  //ボタンテキストのセット

        //ボタン1(最左)のリスナ設定==================================================================================//
        _Button_option_1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String option_selected = Option_texts.get(0);
                Option_texts.clear();   //ボタンテキストにセットするための選択肢たちをリストから削除、そうしないと追加されたままになってしまう
                move_result_view(option_selected);  //選択肢を比較し結果を表示
            }
        });
        //========================================================================================================//
        //ボタン1(真中)のリスナ設定==================================================================================//
        _Button_option_2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String option_selected = Option_texts.get(1);
                Option_texts.clear();   //ボタンテキストにセットするための選択肢たちをリストから削除、そうしないと追加されたままになってしまう
                move_result_view(option_selected);  //選択肢を比較し結果を表示
            }
        });
        //========================================================================================================//
        //ボタン1(最右)のリスナ設定==================================================================================//
        _Button_option_3.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String option_selected = Option_texts.get(2);
                Option_texts.clear();   //ボタンテキストにセットするための選択肢たちをリストから削除、そうしないと追加されたままになってしまう
                move_result_view(option_selected);  //選択肢を比較し結果を表示
            }
        });
        //========================================================================================================//
    }

    //結果画面表示関数================================================================================//
    private  void move_result_view(String _option_selected){
        if(_option_selected.equals(option_right)){
            //正解画面表示(id_random_image, option_right, option_selected)
            Intent intent = new Intent(MainActivity.this, RightActivity.class);
            intent.putExtra("id_image",id_random_image);
            intent.putExtra("text_right", option_right);
            intent.putExtra("text_selected", _option_selected);
            startActivity(intent);
        }else{
            //不正解画面表示(id_random_image, option_right, option_selected)
            Intent intent = new Intent(MainActivity.this, WrongActivity.class);
            intent.putExtra("id_image",id_random_image);
            intent.putExtra("text_right", option_right);
            intent.putExtra("text_selected", _option_selected);
            startActivity(intent);
        }
    }
    //===============================================================================================//

    //選択肢決定関数==================================================================================//
    //概要:ランダムid生成→idに応じた選択肢取得→配列に格納=============================================//
    private void decide_options(int _id_random_image){

        option_right = T_Array_options_right.getString(_id_random_image);    //正解選択肢の取得
        String temp_text_1 = Array_options_wrong.remove(Array_options_wrong.indexOf(option_right));      //正解選択肢に用いた国名を削除

        int num_max_array = Array_options_wrong.size()-1; //Array_options_wrongの要素数を取得
        int id_random_option_1 =(int)Math.floor(Math.random()*num_max_array);   //取得した要素数の範囲内で不正解選択肢1用ランダムid生成
        String option_wrong_1 = Array_options_wrong.get(id_random_option_1);  //不正解選択肢1を取得
        String temp_text_2 = Array_options_wrong.remove(Array_options_wrong.indexOf(option_wrong_1));      //不正解選択肢1に用いた国名を削除

        num_max_array = Array_options_wrong.size()-1; //Array_options_wrongの要素数を取得
        int id_random_option_2 =(int)Math.floor(Math.random()*num_max_array);   //取得した要素数の範囲内で不正解選択肢2用ランダムid生成
        String option_wrong_2 = Array_options_wrong.get(id_random_option_2);  //不正解選択肢2を取得

        Array_options_wrong.add(temp_text_1);  //削除した項目を元に戻す
        Array_options_wrong.add(temp_text_2);

        Option_texts.add(option_right);     //選択肢たちを格納
        Option_texts.add(option_wrong_1);
        Option_texts.add(option_wrong_2);
        Collections.shuffle(Option_texts);  //格納した選択肢たちをシャッフル
    }
    //======================================================================================================//

    //ボタンテキスト設定関数=================================================================================//
    private void set_button_text(){
        _Button_option_1.setText(Option_texts.get(0));
        _Button_option_2.setText(Option_texts.get(1));
        _Button_option_3.setText(Option_texts.get(2));
    }
    //======================================================================================================//
}
