package com.example.quizsample;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

public class RightActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_right);

        ImageView _Iv_quiz = findViewById(R.id.iv_quiz);
        TextView _Text_right = findViewById(R.id.text_right);
        TextView _Text_selected = findViewById(R.id.text_selected);
        Button _Button_next = findViewById(R.id.button_next);

        Intent intent = getIntent();    //mainアクテビティからの値の受け取り
        int id_image = intent.getIntExtra("id_image",0);
        String text_right = intent.getStringExtra("text_right");
        String text_selected = intent.getStringExtra("text_selected");

        //表示する画像の設定============================================================================================//
        TypedArray T_Array_images = getResources().obtainTypedArray(R.array.list_random_image); //画像リスト作成
        Drawable drawable = T_Array_images.getDrawable(id_image);   //リストからid_imageの画像を取得
        _Iv_quiz.setImageDrawable(drawable);    //画像を設定
        //=============================================================================================================//

        //表示する文章の設定===================================================================================//
        _Text_right.setText("正解は" +text_right + "でした");
        _Text_selected.setText("あなたが選んだのは「" +text_selected + "」");
        //====================================================================================================//

        //次の問題へボタンの設定===============================================================================//
        _Button_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(RightActivity.this, MainActivity.class);
                startActivity(intent);
            }
        });
        //====================================================================================================//
    }
}
